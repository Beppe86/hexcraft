using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Guide for this script found here: https://www.youtube.com/watch?v=4HpC--2iowE
public class MovementManager : MonoBehaviour
{

    [SerializeField] private Transform cameraTransform;
    [SerializeField] private CharacterController characterController;
    [SerializeField] [Range(0f, 10f)] private float speed = 3f;
    [SerializeField] [Range(0f, 1f)] private float turnSmoothTime = 0.1f; // Smoothing out the characters turning to different angles when walking
    private float currentSmoothVelocity; // Used by SmoothDampAngle
    
    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal"); // W & A key
        float vertical = Input.GetAxisRaw("Vertical"); // A & D key

        Vector3 direction = new Vector3(horizontal, 0, vertical).normalized; // Normalized to make sure two keys don't make the character go faster

        if (direction.magnitude >= 0.1f) // We're getting an input to move
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cameraTransform.eulerAngles.y; // Finding the angle the character needs to rotate. Camera angle added to move the character with the camera
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref currentSmoothVelocity, turnSmoothTime); // Smoothing out the turning character
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward; // Multiplying with vector3.forward to convert rotation to direction. 
            characterController.Move(moveDirection.normalized * speed * Time.deltaTime); // Time.deltaTime to make it framerate independent.
        }
    }
}
