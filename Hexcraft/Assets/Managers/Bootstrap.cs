using Assets.Grid.Backend;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Managers
{
    public class Bootstrap : MonoBehaviour
    {
        [SerializeField] private GameObject mainCharacter;
        [SerializeField] private int gridStartSize;

        private GridManager gridManager;

        [Inject]
        private void Inject(GridManager gridManager)
        {
            this.gridManager = gridManager;
        }

        private void Awake()
        {
            // Minimum world size to start
            if (gridStartSize < 3)
                gridStartSize = 3;
        }

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            gridManager.CreateGrid(mainCharacter.transform.position, gridStartSize);
        }
    }
}