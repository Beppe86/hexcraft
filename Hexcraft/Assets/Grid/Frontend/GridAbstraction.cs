using Assets.Grid.Backend.Node;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Grid.Frontend
{
    public class GridAbstraction
    {
        private readonly List<Node> nodes;

        public GridAbstraction()
        {
            nodes = new List<Node>();
        }

        public void AddNode(Node node)
        {
            nodes.Add(node);
        }
    }
}