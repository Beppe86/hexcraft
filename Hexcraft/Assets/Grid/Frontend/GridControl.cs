using Assets.Grid.Backend.Node;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Grid.Frontend
{
    public class GridControl
    {
        private GridPresentation gridPresentation;
        private GridAbstraction gridAbstraction;

        public GridControl(GridPresentation gridPresentation, GridAbstraction gridAbstraction)
        {
            this.gridPresentation = gridPresentation;
            this.gridAbstraction = gridAbstraction;
        }
        
        public void AddNewNode(Node node)
        {
            gridAbstraction.AddNode(node);
            gridPresentation.ShowNode(node);
        }
    }
}