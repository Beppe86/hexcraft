using Assets.Grid.Backend.Node;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Grid.Frontend
{
    public class GridPresentation : MonoBehaviour
    {
        [SerializeField] private Hexagon hexagonPrefab;

        private List<Hexagon> hexagons = new List<Hexagon>();

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShowNode(Node node)
        {
            var hexagon = Instantiate(hexagonPrefab, transform);
            hexagon.Initialize(node);

            hexagons.Add(hexagon);
        }
    }
}