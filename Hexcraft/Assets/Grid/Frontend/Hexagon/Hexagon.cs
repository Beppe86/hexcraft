using Assets.Biome;
using Assets.Grid.Backend.Node;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class Hexagon : MonoBehaviour
{
    [SerializeField] private TextMeshPro text;
    [SerializeField] private TextMeshPro coordinates;
    [SerializeField] private float size;
    [SerializeField] private float heightMultiplier;

    [SerializeField] private Material grassMaterial;
    [SerializeField] private Material tundraMaterial;
    [SerializeField] private Material desertMaterial;
    [SerializeField] private Material rainForestMaterial;

    public Vector2Int Coordinates => new Vector2Int(q, r);

    private int q;
    private int r;

    public void Initialize(Node node)
    {
        q = node.Coordinate.x;
        r = node.Coordinate.y;

        var px = size * 1.5f * q;
        var py = size * Mathf.Sqrt(3) * (r + q * 0.5f);

        var height = node.height * heightMultiplier;

        transform.position = new Vector3(px, height, py);
        name = new Vector3(node.Coordinate.x, node.Coordinate.y, node.height).ToString();
        coordinates.text = name;

        node.OnNodeUpdated += SetBiome;
        SetBiome(node);
    }

      public void SetBiome(Node node)
    {
        switch (node.Biome)
        {
            case BiomeTypes.Forest:
                {
                    SetMaterial(grassMaterial);
                }
                break;
            case BiomeTypes.RainForest:
                {
                    SetMaterial(rainForestMaterial);
                }
                break;
            case BiomeTypes.Desert:
                {

                    SetMaterial(desertMaterial);
                }
                break;
            case BiomeTypes.Tundra:
                {
                    SetMaterial(tundraMaterial);
                }
                break;
            default:
                break;
        }

        text.color = Color.yellow;
        text.text = node.Biome.ToString();
    }

    private void SetMaterial(Material material)
    {
        var renderer = GetComponent<MeshRenderer>();
        renderer.material = material;

    }
}
