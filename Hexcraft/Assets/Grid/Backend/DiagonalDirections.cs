﻿using UnityEngine;

namespace Assets.Grid.Backend
{
    public enum DiagonalDirectionEnum
    {
        NorthEast = 0,
        SouthEast = 1,
        South = 2,
        SouthWest = 3,
        NorthWest = 4,
        North = 5,
        None = -1
    }

    public static class DiagonalDirections
    {

        public static DiagonalDirection GetDirection(Vector2Int from, Vector2Int to)
        {
            if (from.x < to.x && from.y == to.y)
                return NorthEast;
            if (from.x < to.x && from.y > to.y)
                return SouthEast;
            if (from.x == to.x && from.y > to.y)
                return South;
            if (from.x > to.x && from.y == to.y)
                return SouthWest;
            if (from.x > to.x && from.y < to.y)
                return NorthWest;
            if (from.x == to.x && from.y < to.y)
                return North;

            return Center;
        }

        private static Vector2Int NorthEastVector = new Vector2Int(1, 0);
        private static Vector2Int SouthEastVector = new Vector2Int(1, -1);
        private static Vector2Int SouthVector = new Vector2Int(0, -1);
        private static Vector2Int SouthWestVector = new Vector2Int(-1, 0);
        private static Vector2Int NorthWestVector = new Vector2Int(-1, 1);
        private static Vector2Int NorthVector = new Vector2Int(0, 1);

        public static DiagonalDirection NorthEast = new DiagonalDirection(DiagonalDirectionEnum.NorthEast, NorthEastVector);
        public static DiagonalDirection SouthEast = new DiagonalDirection(DiagonalDirectionEnum.SouthEast, SouthEastVector);
        public static DiagonalDirection South = new DiagonalDirection(DiagonalDirectionEnum.South, SouthVector);
        public static DiagonalDirection SouthWest = new DiagonalDirection(DiagonalDirectionEnum.SouthWest, SouthWestVector);
        public static DiagonalDirection NorthWest = new DiagonalDirection(DiagonalDirectionEnum.NorthWest, NorthWestVector);
        public static DiagonalDirection North = new DiagonalDirection(DiagonalDirectionEnum.North, NorthVector);
        public static DiagonalDirection Center = new DiagonalDirection(DiagonalDirectionEnum.None, Vector2Int.zero);

        public static DiagonalDirection[] Diagonals = new DiagonalDirection[]
        {
            NorthEast,
            SouthEast,
            South,
            SouthWest,
            NorthWest,
            North
        };
    }

    public class DiagonalDirection
    {
        public DiagonalDirectionEnum Direction;
        public Vector2Int AxialNeighbours;

        public DiagonalDirection(DiagonalDirectionEnum direction, Vector2Int directionVector)
        {
            Direction = direction;
            AxialNeighbours = directionVector;
        }

        public Vector3 ToEuler()
        {
            return new Vector3(0, ((int)Direction * 60) + 60, 0);
        }
    }
}
