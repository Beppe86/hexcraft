﻿using Assets.Biome;
using System.Collections;
using UnityEngine;

namespace Assets.Grid.Backend.Node
{
    public class NodeCharacteristics
    {
        public float Height { get; }
        public BiomeTypes BiomeType { get; set; }

        public NodeCharacteristics(float height)
        {
            Height = height;
            BiomeType = BiomeTypes.Forest;
        }

    }
}