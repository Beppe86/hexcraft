﻿using Assets.Biome;
using System;
using UnityEngine;

namespace Assets.Grid.Backend.Node
{
    public class Node
    {
        public event Action<Node> OnNodeUpdated;

        public Vector2Int Coordinate { get; }
        public float height { get; }

        public BiomeTypes Biome { get; private set; }

        public Node(Vector2Int coordinate, NodeCharacteristics nodeCharacteristics)
        {
            Coordinate = coordinate;
            this.height = nodeCharacteristics.Height;
            Biome = nodeCharacteristics.BiomeType;
        }

        public void SetBiome(BiomeTypes biome)
        {
            Biome = biome;
            OnNodeUpdated?.Invoke(this);
        }

        public override string ToString()
        {
            return Coordinate.ToString();
        }
    }
}
