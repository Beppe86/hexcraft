using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Grid.Backend.Noise
{
    public class NoiseMapConfigurator : MonoBehaviour
    {
        [SerializeField] private NoiseMapCollection noiseMapCollection;
        [SerializeField] private Transform noiseMapViewContainer;
        [SerializeField] private NoiseMapView noiseMapViewPrefab;
        [SerializeField] private BiomePreviewView biomePreviewView;
        [SerializeField] private CloudPreviewView cloudPreviewView;

        private List<NoiseMapView> noiseMapViews = new();
        private int currentIndex = 0;

        public NoiseMapConfiguration CurrentConfiguration;

        private void Awake()
        {
            LoadConfigurations();
            UpdateView();
        }

        public void SaveConfiguration()
        {
            var doesCurrentExistInCollection =
                noiseMapCollection.noiseMapConfigurations.Any(nmc =>
                    nmc.mapInfluence == CurrentConfiguration.mapInfluence);
            if (!doesCurrentExistInCollection)
                noiseMapCollection.noiseMapConfigurations.Add(CurrentConfiguration);
            else
            {
                var existingNoiseMap =
                    noiseMapCollection.noiseMapConfigurations.FirstOrDefault(nmc =>
                        nmc.mapInfluence == CurrentConfiguration.mapInfluence);
                var existingIndex = noiseMapCollection.noiseMapConfigurations.IndexOf(existingNoiseMap);
                noiseMapCollection.noiseMapConfigurations[existingIndex] = CurrentConfiguration;
            }
        }

        public void ResetConfiguration()
        {
            var savedConfiguration =
                noiseMapCollection.noiseMapConfigurations.FirstOrDefault(nmc =>
                    nmc.mapInfluence == CurrentConfiguration.mapInfluence);
            CurrentConfiguration = savedConfiguration;
            UpdateView();
        }

        public void LoadConfigurations()
        {
            for (int i = noiseMapViewContainer.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(noiseMapViewContainer.GetChild(i).gameObject);
            }
            noiseMapViews.Clear();

            foreach (var noiseMapConfiguration in noiseMapCollection.noiseMapConfigurations)
            {
                var noiseMapView = Instantiate(noiseMapViewPrefab, noiseMapViewContainer);
                noiseMapView.ApplyConfiguration(noiseMapConfiguration);

                noiseMapViews.Add(noiseMapView);
            }

            SelectIndex(0);
        }

        public void SelectNext()
        {
            currentIndex = ++currentIndex % noiseMapViews.Count;
            SelectIndex(currentIndex);
        }

        public void SelectPrevious()
        {
            currentIndex = --currentIndex % noiseMapViews.Count;
            SelectIndex(currentIndex);
        }

        private void SelectIndex(int index)
        {
            if (index >= noiseMapViews.Count)
                return;

            currentIndex = index;

            foreach (var noiseMapView in noiseMapViews.Where(nmv => nmv.indicator.activeSelf))
            {
                noiseMapView.indicator.SetActive(false);
            }

            noiseMapViews[index].indicator.SetActive(true);

            CurrentConfiguration = new NoiseMapConfiguration(noiseMapViews[index].noiseMap, Enum.Parse<MapInfluence>(noiseMapViews[index].title.text));
        }

        private void OnValidate()
        {
            UpdateView();
        }

        private void UpdateView()
        {
            var currentView = noiseMapViews[currentIndex];
            currentView.ApplyConfigurationValuesOnly(CurrentConfiguration);
        }

        public void RefreshBiomePreview()
        {
            biomePreviewView.Generate();
        }

        public void RefreshCloudPreview()
        {
            cloudPreviewView.RefreshClouds();
        }
    }
}