using Grid.Backend.Noise;
using UnityEngine;

public class NoiseMapGenerator
{
    private readonly int seed;
    private readonly float scale;
    private readonly int octaves;
    private readonly float persistence;
    private readonly float lacunarity;

    private readonly System.Random rand;
    private readonly Vector2[] octavesOffset;

    public NoiseMapGenerator(NoiseMapConfiguration noiseSettings, int seed)
    {
        scale = noiseSettings.scale;
        seed = 0;
        octaves = noiseSettings.octaves;
        persistence = noiseSettings.persistence;
        lacunarity = noiseSettings.lacunarity;

        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        // Seed element
        rand = new System.Random(seed);

        octavesOffset = new Vector2[octaves];

        // Octave shift to get a more interesting picture with overlapping
        for (int i = 0; i < octaves; i++)
        {
            // Also use external position shift
            float xOffset = rand.Next(-100000, 100000);
            float yOffset = rand.Next(-100000, 100000);
            octavesOffset[i] = new Vector2(xOffset, yOffset);
        }
    }

    public float[] GetNoiseMap(int width, int height, Vector2Int offset)
    {
        // An array of vertex data, a one-dimensional view will help get rid of unnecessary cycles later
        float[] noiseMap = new float[width * height];

        // For a more visually pleasant zoom shift our view to the center
        float halfWidth = width / 2f;
        float halfHeight = height / 2f;

        // Generate points for a heightmap
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                var coord = new Vector2(x - halfWidth, y - halfHeight);

                // Save heightmap point
                noiseMap[y * width + x] = GetCoordinateNoise(coord, offset);
            }
        }

        return noiseMap;
    }

    public float GetCoordinateNoise(Vector2 coordinate)
    {
        return GetCoordinateNoise(coordinate, Vector2.zero);
    }

    public float GetCoordinateNoise(Vector2 coordinate, Vector2 offset)
    {
        // Set the values for the first octave
        float amplitude = 1;
        float frequency = 1;
        float noiseHeight = 0;
        float superpositionCompensation = 0;

        // Octave Overlay Processing
        for (int i = 0; i < octaves; i++)
        {
            // Calculate the coordinates to get from Noise Perlin
            float xResult = coordinate.x / scale * frequency + (octavesOffset[i].x + offset.x) * frequency;
            float yResult = coordinate.y / scale * frequency + (octavesOffset[i].y + offset.y) * frequency;

            // Obtaining Altitude from the PRNG
            float generatedValue = Mathf.PerlinNoise(xResult, yResult);
            // Octave overlay
            noiseHeight += generatedValue * amplitude;
            // Compensate octave overlay to stay in a range [0,1]
            noiseHeight -= superpositionCompensation;

            // Calculation of amplitude, frequency and superposition compensation for the next octave
            amplitude *= persistence;
            frequency *= lacunarity;
            superpositionCompensation = amplitude / 2;
        }

        // Due to the superposition of octaves, there is a chance of going out of the range [0,1]
        return Mathf.Clamp01(noiseHeight);
    }
}