using System.Collections.Generic;
using UnityEngine;

namespace Grid.Backend.Noise
{
    [CreateAssetMenu(fileName = "NoiseMapCollection", menuName = "ScriptableObjects/NoiseMapCollection")]
    public class NoiseMapCollection : ScriptableObject
    {
        [SerializeField] public List<NoiseMapConfiguration> noiseMapConfigurations;
    }

    public enum MapInfluence
    {
        Height,
        Temperature,
        Precipitation,
        Clouds,
        EvapoTranspiration
    }
}