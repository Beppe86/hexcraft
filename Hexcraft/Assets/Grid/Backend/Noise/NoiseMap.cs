using System;
using UnityEngine;
using UnityEngine.UI;

namespace Grid.Backend.Noise
{
    public enum MapType
    {
        Noise,
        Color
    }

    public class NoiseMap : MonoBehaviour
    {
        // Input data for our noise generator
        [SerializeField] public int width;
        [SerializeField] public int height;
        [SerializeField] public float scale;

        [Range(0, 10)]
        [SerializeField] public int octaves;
        [Range(0, 10)]
        public float persistence;
        [Range(0, 10)]
        public float lacunarity;

        [SerializeField] public int seed;
        [SerializeField] public Vector2Int offset;

        private RectTransform rectTransform;

        private void Awake()
        {
            SetSize();
        }

        public void SetSize()
        {
            if (rectTransform == null)
                rectTransform = GetComponentInParent<RectTransform>();
            if (rectTransform == null)
                return;
            
            width = Mathf.RoundToInt(rectTransform.sizeDelta.x);
            height = Mathf.RoundToInt(rectTransform.sizeDelta.y);
            var thisRectTransform = GetComponent<RectTransform>();
            thisRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            thisRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);

            LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
            LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
        }

        private void OnValidate()
        {
            SetSize();
            GenerateMap();
        }

        public void GenerateMap()
        {
            var config = new NoiseMapConfiguration()
            {
                octaves = octaves,
                lacunarity = lacunarity,
                persistence = persistence,
                scale = scale,
            };

            // Generate a map
            var generator = new NoiseMapGenerator(config, seed);
            float[] noiseMap = generator.GetNoiseMap(width, height, offset); // NoiseMapGenerator.GenerateNoiseMap(width, height, seed, scale, octaves, persistence, lacunarity, offset);

            // Pass the map to the renderer
            NoiseMapRenderer mapRenderer = GetComponent<NoiseMapRenderer>();
            mapRenderer.RenderMap(width, height, noiseMap);
        }
    }
}