using TMPro;
using UnityEngine;

namespace Grid.Backend.Noise
{
    public class NoiseMapView : MonoBehaviour
    {
        public TextMeshProUGUI title;
        public GameObject indicator;
        public NoiseMap noiseMap;

        public void ApplyConfiguration(NoiseMapConfiguration noiseMapConfiguration)
        {
            title.text = noiseMapConfiguration.mapInfluence.ToString();
            
            ApplyConfigurationValuesOnly(noiseMapConfiguration);
        }

        public void ApplyConfigurationValuesOnly(NoiseMapConfiguration noiseMapConfiguration)
        {
            noiseMap.octaves = noiseMapConfiguration.octaves;
            noiseMap.scale = noiseMapConfiguration.scale;
            noiseMap.persistence = noiseMapConfiguration.persistence;
            noiseMap.lacunarity = noiseMapConfiguration.lacunarity;
            
            noiseMap.SetSize();
            noiseMap.GenerateMap();
        }
    }
}