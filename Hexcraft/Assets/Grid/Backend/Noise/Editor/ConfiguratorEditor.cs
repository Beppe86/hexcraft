using UnityEditor;
using UnityEngine;

namespace Grid.Backend.Noise.Editor
{
    [CustomEditor(typeof(NoiseMapConfigurator))]
    public class ConfiguratorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var configurator = (NoiseMapConfigurator) target;
            
            if (GUILayout.Button("Load configurations"))
            {
                configurator.LoadConfigurations();
            }
            
            if (GUILayout.Button("Save configuration"))
            {
                configurator.SaveConfiguration();
            }
            
            if (GUILayout.Button("Reset configuration"))
            {
                configurator.ResetConfiguration();
            }
            
            if (GUILayout.Button("Next"))
            {
                configurator.SelectNext();
            }
            
            if (GUILayout.Button("Previous"))
            {
                configurator.SelectPrevious();
            }

            if (GUILayout.Button("Refresh Biome Preview"))
            {
                configurator.RefreshBiomePreview();
            }

            if (GUILayout.Button("Refresh Cloud Preview"))
            {
                configurator.RefreshCloudPreview();
            }
        }
    }
}