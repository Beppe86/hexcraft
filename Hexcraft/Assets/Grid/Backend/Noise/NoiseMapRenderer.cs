using UnityEngine;
using UnityEngine.UI;

namespace Grid.Backend.Noise
{
    public class NoiseMapRenderer : MonoBehaviour
    {
        [SerializeField] public Image image;

        public void RenderMap(int width, int height, float[] noiseMap)
        {
            ApplyColorMap(width, height, GenerateNoiseMap(noiseMap));
        }

        // Create texture and sprite to display
        private void ApplyColorMap(int width, int height, Color[] colors)
        {
            Texture2D texture = new Texture2D(width, height);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Point;
            texture.SetPixels(colors);
            texture.Apply();

            image.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f); ;
        }

        // Convert an array with noise data into an array of black and white colors, for transmission to the texture
        private Color[] GenerateNoiseMap(float[] noiseMap)
        {
            Color[] colorMap = new Color[noiseMap.Length];
            for (int i = 0; i < noiseMap.Length; i++)
            {
                colorMap[i] = Color.Lerp(Color.black, Color.white, noiseMap[i]);
            }
            return colorMap;
        }
    }
}