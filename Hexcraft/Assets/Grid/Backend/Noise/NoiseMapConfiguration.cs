using System;
using UnityEngine;

namespace Grid.Backend.Noise
{
    [Serializable]
    public struct NoiseMapConfiguration
    {
        public MapInfluence mapInfluence;
        [Range(1, 10)]
        public int octaves;
        [Range(0, 1000)]
        public float scale;
        [Range(0, 10)]
        public float persistence;
        [Range(0, 10)]
        public float lacunarity;

        public NoiseMapConfiguration(NoiseMap noiseMap, MapInfluence mapInfluence)
        {
            this.mapInfluence = mapInfluence;
            octaves = noiseMap.octaves;
            scale = noiseMap.scale;
            persistence = noiseMap.persistence;
            lacunarity = noiseMap.lacunarity;
        }
    }
}