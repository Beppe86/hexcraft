using Assets.Biome;
using Grid.Backend.Noise;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Grid.Backend.Node;
using UnityEngine;
using UnityEngine.UI;

public class BiomePreviewView : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private NoiseMapCollection noiseMapCollection;
    [SerializeField] private BiomePropertyRanges biomePropertyRanges;

    public void Generate()
    {
        var biomeResolver = new BiomeResolver(biomePropertyRanges);
        var noiseResolver = new NoiseResolver(0, biomeResolver, noiseMapCollection);

        int width = Mathf.RoundToInt(image.rectTransform.sizeDelta.x);
        int height = Mathf.RoundToInt(image.rectTransform.sizeDelta.y);

        var texture = new Texture2D(width, height);
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.filterMode = FilterMode.Point;

        for (var y = 0; y < height; y++)
        {
            for (var x = 0; x < width; x++)
            {
                var characteristics = noiseResolver.GetCharacteristics(new Vector2Int(x, y));
                var biome = biomePropertyRanges.BiomeDefinitions.FirstOrDefault(b => b.BiomeType == characteristics.BiomeType);
                var biomeColor = biome.Material?.color ?? Color.magenta;
                
                if (characteristics.Height < .3)
                {
                    biomeColor = SetWaterColor(biomeColor);
                }
                else if (characteristics.Height > .5)
                {
                    biomeColor = SetPeakColor(biomeColor, characteristics.Height);
                }
                
                biomeColor = SetHeightBrightness(biomeColor, characteristics.Height);

                texture.SetPixel(x, y, biomeColor);
            }
        }

        texture.Apply();
        image.sprite = Sprite.Create(texture, new Rect(0, 0, width, height), new Vector2(0.5f, 0.5f), 100);
    }

    private static Color SetHeightBrightness(Color biomeColor, float height)
    {
        Color.RGBToHSV(biomeColor, out var h, out var s, out var v);
        biomeColor = Color.HSVToRGB(h, s, v * Mathf.Lerp(.5f, 1.75f, height));
        return biomeColor;
    }

    private static Color SetWaterColor(Color biomeColor)
    {
        Color.RGBToHSV(biomeColor, out var hue, out var saturation, out var value);
        biomeColor = Color.HSVToRGB(196f / 360, 0.85f, value);
        return biomeColor;
    }

    private static Color SetPeakColor(Color biomeColor, float height)
    {
        Color.RGBToHSV(biomeColor, out var hue, out var saturation, out var value);
        biomeColor = Color.HSVToRGB(hue, saturation / Mathf.Lerp(1, 2, height), value);
        return biomeColor;
    }
}
