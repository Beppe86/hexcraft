using System;
using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Grid.Backend
{
    // This component returns the hexagon to the pool when the hexagon leaves the collider of the main character
    [RequireComponent(typeof(Hexagon))]
    public class ReturnToPool : MonoBehaviour
    {
        public Hexagon hexagon;
        public IObjectPool<Hexagon> pool;

        void Start()
        {
            hexagon = GetComponent<Hexagon>();
            // TODO Maybe stuff should happen here with the variables in the hexagon??
        }

        void OnLeavingMainCharacterCollider()
        {
            // Return to the pool
            pool.Release(hexagon);
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("OnTriggerEnter");
        }

        private void OnTriggerExit(Collider other) 
        {
            Debug.Log("OnTriggerExit");
            OnLeavingMainCharacterCollider();
        }
    }
    
    public class HexagonPoolManager : MonoBehaviour
    {
        // Collection checks will throw errors if we try to release an item that is already in the pool
        [SerializeField] private bool collectionChecks = true;
        [SerializeField] private int maxPoolSize = 1000;

        IObjectPool<Hexagon> hexagonPool;

        public IObjectPool<Hexagon> HexagonPool
        {
            get
            {
                hexagonPool = hexagonPool = new ObjectPool<Hexagon>(CreatePooledHexagon, OnTakeFromPool,
                    OnReturnedToPool, OnDestroyPoolObject, collectionChecks, 10, maxPoolSize);
                return hexagonPool;
            }
        }

        // Creates a hexagon GameObject that will be returned to the pool when not used anymore
        Hexagon CreatePooledHexagon()
        {
            GameObject gameObject = new GameObject("Pooled Hexagon");
            Hexagon hexagon = gameObject.AddComponent<Hexagon>();
            // TODO Clear the hexagon so it's ready for instantiation

            // This is used to return Hexagon to the pool when not used anymore
            ReturnToPool returnToPool = gameObject.AddComponent<ReturnToPool>();
            returnToPool.pool = HexagonPool;

            return hexagon;
        }
        
        // Called when a hexagon is returned to the pool using Release
        void OnReturnedToPool(Hexagon hexagon)
        {
            hexagon.gameObject.SetActive(false);
        }
        
        // Called when a hexagon is taken from the pool using Get
        void OnTakeFromPool(Hexagon hexagon)
        {
            hexagon.gameObject.SetActive(true);
        }
        
        // If the pool capacity is reached, any items returned will be destroyed.
        // Here we can destroy the object exactly as we'd like
        void OnDestroyPoolObject(Hexagon hexagon)
        {
            Destroy(hexagon.gameObject);
        }
    }
}

