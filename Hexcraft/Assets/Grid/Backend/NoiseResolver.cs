using Assets.Biome;
using Assets.Grid.Backend.Node;
using Grid.Backend.Noise;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NoiseResolver
{
    private readonly BiomeResolver biomeResolver;

    private readonly NoiseMapGenerator heightMap;
    private readonly NoiseMapGenerator precipitationMap;
    private readonly NoiseMapGenerator temperatureMap;

    public NoiseResolver(int seed, BiomeResolver biomeResolver, NoiseMapCollection noiseMapCollection)
    {
        this.biomeResolver = biomeResolver;

        var heightConfig = noiseMapCollection.noiseMapConfigurations.FirstOrDefault(map => map.mapInfluence == MapInfluence.Height);
        heightMap = new NoiseMapGenerator(heightConfig, seed);

        var precipitationConfig = noiseMapCollection.noiseMapConfigurations.FirstOrDefault(map => map.mapInfluence == MapInfluence.Precipitation);
        precipitationMap = new NoiseMapGenerator(precipitationConfig, seed);

        var temperatureConfig = noiseMapCollection.noiseMapConfigurations.FirstOrDefault(map => map.mapInfluence == MapInfluence.Temperature);
        temperatureMap = new NoiseMapGenerator(temperatureConfig, seed);
    }

    public NodeCharacteristics GetCharacteristics(Vector2Int coordinate)
    {
        var height = heightMap.GetCoordinateNoise(coordinate);
        var precipitation = precipitationMap.GetCoordinateNoise(coordinate);
        var temperature = temperatureMap.GetCoordinateNoise(coordinate);

        return biomeResolver.ResolveNodeCharacteristics(height, precipitation, temperature);
    }
}
