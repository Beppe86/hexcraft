using Assets.Grid.Backend.Node;
using Assets.Grid.Frontend;
using Assets.Managers;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Assets.Grid.Backend
{
    public class GridManager
    {
        private readonly GridControl gridControl;
        private readonly NoiseResolver noiseResolver;
        private readonly Dictionary<Vector2Int, Node.Node> grid;

        public GridManager(GridControl gridControl, NoiseResolver noiseResolver)
        {
            grid = new Dictionary<Vector2Int, Node.Node>();
            this.gridControl = gridControl;
            this.noiseResolver = noiseResolver;
        }

        public void CreateGrid(Vector3 position, int size)
        {
            Vector2Int characterPosition = PositionToHexCoordinate(position);
            
            Debug.Log($"Getting coordinates around: {characterPosition}");
            
            var neighbours = CoordinateHelper.GetCoordinatesAround(characterPosition, size, true);
            
            foreach (var coordinate in neighbours)
            {
                if(grid.ContainsKey(coordinate))
                    continue;
                
                var nodeCharacteristics = noiseResolver.GetCharacteristics(coordinate);
                var node = new Node.Node(coordinate, nodeCharacteristics);
                grid.Add(coordinate, node);
                gridControl.AddNewNode(node);
            }
        }

        public Vector2Int PositionToHexCoordinate(Vector3 position)
        {
            float hexSize = 0.51f; // Hexagon size 
            
            // World position to hexagon coordinates:
            var q = (Mathf.Sqrt(3f) / 3 * position.x - 1f / 3 * (position.z * -1)) / hexSize;
            var s = ( 2f/3 * (position.z * -1)) / hexSize;
            
            Vector3 cubeCoordinates = AxialToCube(new Vector2(q,s));

            Vector3Int roundedCubeCoordinates = CubeRound(cubeCoordinates);

            var qRounded = roundedCubeCoordinates.x;
            var rRounded = roundedCubeCoordinates.y;
            
            Debug.Log($"Coordinate q: {qRounded}, r: {rRounded}");
            
            return new Vector2Int(qRounded, rRounded);
        }

        Vector3 AxialToCube(Vector2 hexagonCoordinates)
        {
            float q = hexagonCoordinates.x;
            float s = hexagonCoordinates.y;
            float r = -q - s;

            return new Vector3(q, r, s);
        }

        Vector3Int CubeRound(Vector3 cubeCoord)
        {
            int q = Mathf.RoundToInt(cubeCoord.x);
            int r = Mathf.RoundToInt(cubeCoord.y);
            int s =Mathf.RoundToInt(cubeCoord.z);

            var q_diff = Mathf.Abs(q - cubeCoord.x);
            var r_diff = Mathf.Abs(r - cubeCoord.y);
            var s_diff = Mathf.Abs(s - cubeCoord.z);
            
            if (q_diff > r_diff && q_diff > s_diff)
            {
                q = -r - s;
            }
            else if (r_diff > s_diff)
            {
                r = -q - s;
            }
            else
            {
                s = -q - r;
            }

            return new Vector3Int(q, r, s);
        }
        
    }
}