﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Grid.Backend
{
    public static class CoordinateHelper
    {
        /// <summary>
        /// Returns a list of coordinates inside a certain range around given coordinate
        /// </summary>
        /// <param name="coordinate">The central coordinate around which to get neighbouring nodes</param>
        /// <param name="radius">The radius witch to add coordinates inside of</param>
        /// <param name="includeCentralNode">Should the central node be included, It will be added first in the list if so</param>
        /// <returns>Returns List of Vector2Int</returns>
        public static IEnumerable<Vector2Int> GetCoordinatesAround(Vector2Int coordinate, int radius, bool includeCentralNode)
        {
            if (radius < 1) radius = 1;
            if (radius == 1) return GetNeighbours(coordinate, includeCentralNode);

            List<Vector2Int> coordinates = new List<Vector2Int>();

            for (int q = -radius; q <= radius; q++)
                for (int r = Mathf.Max(-radius, (-q - radius)); r <= Mathf.Min(radius, -q + radius); r++)
                {
                    coordinates.Add(new Vector2Int(q, r) + coordinate);
                }

            if (includeCentralNode == false)
                coordinates.Remove(coordinate);

            return coordinates;
        }

        /// <summary> Return a list of coordinates in a specified range around the given node.
        /// The returned list is in no specific order. </summary>
        /// <param name="coordinate">		The central coordinate. </param>
        /// <param name="radius">	Radius from central coordinate that ring starts. </param>
        /// <param name="width">	Width of the ring. </param>
        /// <returns>List of Vector2Int</returns>
        public static IEnumerable<Vector2Int> CoordinatesRing(Vector2Int coordinate, int radius, int width)
        {
            if (radius < 1) radius = 1;
            if (width < 1) width = 1;
            if (radius == 1)
            {
                if (width == 1)
                    return GetNeighbours(coordinate, false);
                else
                    return GetCoordinatesAround(coordinate, width, false);
            }

            List<Vector2Int> coordinates = new List<Vector2Int>();
            var positionOnRing = coordinate + (DiagonalDirections.Diagonals[4].AxialNeighbours * radius);

            for (var deltaWidth = 0; deltaWidth < width; deltaWidth++)
            {
                for (var dir = 0; dir < 6; dir++)
                {
                    for (var step = 0; step < radius + deltaWidth; step++)
                    {
                        coordinates.Add(positionOnRing);
                        positionOnRing = positionOnRing + DiagonalDirections.Diagonals[dir].AxialNeighbours;
                    }
                }
                positionOnRing = coordinate + DiagonalDirections.Diagonals[2].AxialNeighbours;
            }
            return coordinates;
        }

        public static int DistanceInNodes(Vector2Int pointA, Vector2Int pointB)
        {
            if (pointA == pointB) return 0;

            return (Mathf.Abs(pointA.x - pointB.x) +
                    Mathf.Abs(pointA.y - pointB.y) +
                    Mathf.Abs(pointA.x + pointA.y - pointB.x - pointB.y)) / 2;
        }

        /// <summary>
        /// Gets all neighbour coordinates of a coordinate, order will be anti clockwise but this is not validated and should not be trusted
        /// </summary>
        /// <param name="coordinate">Coordinates of hexagon</param>
        /// <returns>Returns a list neighbouring coordinates</returns>
        private static IEnumerable<Vector2Int> GetNeighbours(Vector2Int coordinate, bool includeCentralNode)
        {
            if (includeCentralNode)
                yield return coordinate;

            foreach (var diagonal in DiagonalDirections.Diagonals)
                yield return coordinate + diagonal.AxialNeighbours;

        }
    }
}
