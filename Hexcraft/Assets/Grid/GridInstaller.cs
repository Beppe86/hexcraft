using System.ComponentModel;
using Assets.Biome;
using Assets.Grid.Backend;
using Assets.Grid.Frontend;
using Grid.Backend.Noise;
using UnityEngine;
using Zenject;

namespace Assets.Grid
{
    public class GridInstaller : MonoInstaller
    {
        [SerializeField] private GridPresentation presentation;
        [SerializeField] private int seed;

        [SerializeField] private NoiseMapCollection noiseMapCollection;
        [SerializeField] private BiomePropertyRanges biomePropertyRanges;

        public override void InstallBindings()
        {
            Container.Bind<GridPresentation>()
                .FromComponentInNewPrefab(presentation)
                .AsSingle();

            Container.BindInterfacesAndSelfTo<GridAbstraction>().AsSingle();
            Container.BindInterfacesAndSelfTo<GridControl>().AsSingle();

            var biomeResolver = new BiomeResolver(biomePropertyRanges);
            var noiseResolver = new NoiseResolver(seed, biomeResolver, noiseMapCollection);

            Container.Bind<NoiseResolver>().FromInstance(noiseResolver);

            Container.Bind<GridManager>().AsSingle();
        }
    }
}