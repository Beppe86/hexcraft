using System;
using System.Collections;
using UnityEngine;

namespace Assets.Visuals
{
    public class DayNightCycle : MonoBehaviour
    {
        [SerializeField] private Light light = null;
        [SerializeField] private Transform lightSourceTransform = null;
        [SerializeField] private Material skyboxMaterial = null;
        [SerializeField] private float speed = 1f;

        [SerializeField] private TimeOfDay morningToDay;
        [SerializeField] private TimeOfDay dayToEvening;
        [SerializeField] private TimeOfDay eveningToNight;
        [SerializeField] private TimeOfDay nightToMorning;

        private Quaternion initialLightSourceRotation;
        private static readonly int SkyGradientBottomShaderProperty = Shader.PropertyToID("_SkyGradientBottom");

        private void Awake()
        {
            initialLightSourceRotation = lightSourceTransform.rotation;
        }

        private void OnEnable()
        {
            StartCoroutine(nameof(Animate));
        }

        private void OnDisable()
        {
            StopCoroutine(nameof(Animate));
        }

        private IEnumerator Animate()
        {
            while (true)
            {
                yield return AnimateTimeOfDay(dayToEvening);
                yield return AnimateTimeOfDay(eveningToNight);
                yield return AnimateTimeOfDay(nightToMorning);
                yield return AnimateTimeOfDay(morningToDay);
            }
        }

        private IEnumerator AnimateTimeOfDay(TimeOfDay timeOfDay)
        {
            var factor = 0f;
            while (factor < 1)
            {
                SetTimeOfDay(timeOfDay, factor);
                yield return null;
                factor += Time.deltaTime * speed;
            }
            
            SetTimeOfDay(timeOfDay, 1f);
        }

        private void SetTimeOfDay(TimeOfDay timeOfDay, float factor)
        {
            lightSourceTransform.rotation = initialLightSourceRotation *
                                            Quaternion.Euler(0f, timeOfDay.GetAngle(factor), 0f);
            timeOfDay.Lerp(skyboxMaterial, factor);
            RenderSettings.fogColor = skyboxMaterial.GetColor(SkyGradientBottomShaderProperty);
            light.intensity = timeOfDay.GetIntensity(factor);
        }

        [Serializable]
        private class TimeOfDay
        {
            [Header("Skybox")]
            [SerializeField] private Material fromMaterial;
            [SerializeField] private Material toMaterial;
            [SerializeField] private bool easeOut = false;
            [Header("Light Source")]
            [SerializeField] private Vector2 angleRange;
            [SerializeField] private Vector2 intensityRange;

            public float GetIntensity(float factor)
            {
                return Mathf.Lerp(intensityRange.x, intensityRange.y, GetModifiedFactor(factor));
            }
            
            public float GetAngle(float factor)
            {
                return Mathf.Lerp(angleRange.x, angleRange.y, factor);
            }
            
            public void Lerp(Material material, float factor)
            {
                factor = GetModifiedFactor(factor);
                material.Lerp(fromMaterial, toMaterial, factor);
            }

            private float GetModifiedFactor(float factor)
            {
                if (easeOut) factor = 1 - factor;
                factor *= factor;
                if (easeOut) factor = 1 - factor;
                return factor;
            }
        }
    }
}