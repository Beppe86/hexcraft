#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Assets.Visuals
{
    [RequireComponent(typeof(Renderer))]
    public class SceneViewVisibleOnly : MonoBehaviour
    {
#if UNITY_EDITOR
        private Renderer cacheRenderer = null;

        private void Awake()
        {
            cacheRenderer = GetComponent<Renderer>();
            Camera.onPreCull += Camera_OnPreCull;
        }

        private void OnDestroy()
        {
            Camera.onPreCull -= Camera_OnPreCull;
        }
        
        private void Camera_OnPreCull(Camera cam)
        {
            cacheRenderer.enabled = SceneView.currentDrawingSceneView != null;
        }
#endif
    }
}