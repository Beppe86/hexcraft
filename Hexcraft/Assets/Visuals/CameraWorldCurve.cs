using UnityEngine;

namespace Assets.Visuals
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class CameraWorldCurve : MonoBehaviour
    {
        private static readonly int ShaderQOffsetId = Shader.PropertyToID("_QOffset");
        private static readonly int ShaderDistId = Shader.PropertyToID("_Dist");

        [SerializeField] private Vector4 qOffset = new Vector4();
        [SerializeField] private float dist = 0f;

        private static readonly Vector4 ZeroQOffset = new Vector4();
        private const float DefaultDist = 10000;

        private void OnPreRender()
        {
            Shader.SetGlobalVector(ShaderQOffsetId, qOffset);
            Shader.SetGlobalFloat(ShaderDistId, dist);
        }

        private void OnPostRender()
        {
            Shader.SetGlobalVector(ShaderQOffsetId, ZeroQOffset);
            Shader.SetGlobalFloat(ShaderDistId, DefaultDist);
        }
    }
}