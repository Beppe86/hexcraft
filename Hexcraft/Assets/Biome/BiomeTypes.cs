﻿using System.Collections;
using UnityEngine;

namespace Assets.Biome
{
    public enum BiomeTypes
    {
        RainForest,
        Desert,
        Tundra,
        Forest,
        GrassLand,
        Snow
    }
}