﻿using System;
using UnityEngine;

namespace Assets.Biome
{
    [Serializable]
    public struct BiomeDefinition
    {
        public BiomeTypes BiomeType;

        public float LowestPrecipitation;
        public float HighestPrecipitation;
        public float LowestTemperature;
        public float HighestTemperature;
        public Material Material;

        public bool IsInsidePrecipitation(float precipitation)
        {
            return LowestPrecipitation <= precipitation && HighestPrecipitation > precipitation;
        }

        public bool IsInsideTemperature(float temperature)
        {
            return LowestTemperature <= temperature && HighestTemperature > temperature;
        }
    }
}