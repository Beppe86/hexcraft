﻿using System.Collections;
using UnityEngine;

namespace Assets.Biome
{
    [CreateAssetMenu(fileName = "BiomePropertyRange", menuName = "ScriptableObjects/Biome/BiomePropertyRange")]
    public class BiomePropertyRanges : ScriptableObject
    {
        public Vector2 PrecipitationRange;
        public Vector2 TemperatureRange;
        public BiomeDefinition[] BiomeDefinitions;
    }
}