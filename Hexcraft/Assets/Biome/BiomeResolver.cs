﻿using Assets.Grid.Backend.Node;
using System.Collections;
using UnityEngine;

namespace Assets.Biome
{
    public class BiomeResolver
    {
        private readonly BiomePropertyRanges biomePropertyRanges;

        public BiomeResolver(BiomePropertyRanges biomePropertyRanges)
        {
            this.biomePropertyRanges = biomePropertyRanges;
        }

        public NodeCharacteristics ResolveNodeCharacteristics(float height, float precipitationNormalized, float temperatureNormalized)
        {
            var precipitationLerped = Mathf.Lerp(Mathf.Log(biomePropertyRanges.PrecipitationRange.x), Mathf.Log(biomePropertyRanges.PrecipitationRange.y), precipitationNormalized);
            var precipitationPow = Mathf.Pow(2, precipitationLerped);

            var temperatureLerped = Mathf.Lerp(Mathf.Log(biomePropertyRanges.TemperatureRange.x), Mathf.Log(biomePropertyRanges.TemperatureRange.y), temperatureNormalized);
            var temperaturePow = Mathf.Pow(2, temperatureLerped);

            var NodeCharacteristics = new NodeCharacteristics(height);

            foreach (var biomeDef in biomePropertyRanges.BiomeDefinitions)
            {
                if (biomeDef.IsInsidePrecipitation(precipitationPow) && biomeDef.IsInsideTemperature(temperaturePow))
                    NodeCharacteristics.BiomeType = biomeDef.BiomeType;
            }

            return NodeCharacteristics;
        }
    }
}