using System.Linq;
using Grid.Backend.Noise;
using UnityEngine;
using UnityEngine.UI;

public class CloudPreviewView : MonoBehaviour
{
    public Image image;
    public NoiseMapCollection NoiseMapCollection;
    private Vector2 currentPosition = Vector2.zero;
    private int width;
    private int height;

    private float timer = 0;
    private float refreshRate = 0.1f;
    
    private NoiseMapGenerator noiseMapGenerator;

    private void OnRectTransformDimensionsChange()
    {
        width = Mathf.RoundToInt(image.rectTransform.sizeDelta.x);
        height = Mathf.RoundToInt(image.rectTransform.sizeDelta.y);
    }

    void Update()
    {
        if (timer <= refreshRate)
        {
            timer += Time.deltaTime;
            return;
        }

        timer = 0;
        
        RefreshClouds();
    }

    public void RefreshClouds()
    {
        if (true)
            noiseMapGenerator =
                new NoiseMapGenerator(
                    NoiseMapCollection.noiseMapConfigurations.First(nmc => nmc.mapInfluence == MapInfluence.Clouds), 0);

        if (width == 0 || height == 0)
        {
            width = Mathf.RoundToInt(image.rectTransform.sizeDelta.x);
            height = Mathf.RoundToInt(image.rectTransform.sizeDelta.y);
        }


        currentPosition += Vector2.up + Vector2.right;

        var texture = new Texture2D(width, height)
        {
            wrapMode = TextureWrapMode.Clamp,
            filterMode = FilterMode.Point
        };
        for (var y = 0; y < height; y++)
        {
            for (var x = 0; x < width; x++)
            {
                var noise = noiseMapGenerator.GetCoordinateNoise(new Vector2(x, y) + currentPosition);
                texture.SetPixel(x, y, new Color(1, 1, 1, noise));
            }
        }

        texture.Apply();
        image.sprite = Sprite.Create(texture, new Rect(0, 0, width, height), new Vector2(.5f, .5f), 100);
    }
}
